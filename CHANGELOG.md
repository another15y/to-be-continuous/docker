# [6.0.0](https://gitlab.com/to-be-continuous/docker/compare/5.14.1...6.0.0) (2024-11-26)


### Code Refactoring

* **trivy:** enforce usage of Trivy environment variables ([e69ce13](https://gitlab.com/to-be-continuous/docker/commit/e69ce13565711b9dac8ee7b7105c8e9061bb3799))


### BREAKING CHANGES

* **trivy:** 4 Trivy configuration params removed in favor of the native Trivy environment variables

## [5.14.1](https://gitlab.com/to-be-continuous/docker/compare/5.14.0...5.14.1) (2024-11-02)


### Bug Fixes

* limit security reports access to developer role or higher ([29a3173](https://gitlab.com/to-be-continuous/docker/commit/29a3173e7393ad84df324a34cd8cdc17ab3cac14))

# [5.14.0](https://gitlab.com/to-be-continuous/docker/compare/5.13.3...5.14.0) (2024-11-01)


### Features

* **mirror:** add support for DOCKER_REGISTRY_MIRROR_USER/PASSWORD vars ([76c9d16](https://gitlab.com/to-be-continuous/docker/commit/76c9d1699fb5a6c03b827c8b035314e4873d173b))

## [5.13.3](https://gitlab.com/to-be-continuous/docker/compare/5.13.2...5.13.3) (2024-10-24)


### Bug Fixes

* set trivy artifact expiration ([5854b42](https://gitlab.com/to-be-continuous/docker/commit/5854b428f1007c7cde90ad836fba6b9cbcc2a251))

## [5.13.2](https://gitlab.com/to-be-continuous/docker/compare/5.13.1...5.13.2) (2024-10-04)


### Bug Fixes

* **trivy:** use --pkg-types instead of deprecated --vuln-type option ([76d0f3f](https://gitlab.com/to-be-continuous/docker/commit/76d0f3f506278028eebb3a526322ae205f347761))

## [5.13.1](https://gitlab.com/to-be-continuous/docker/compare/5.13.0...5.13.1) (2024-09-21)


### Bug Fixes

* allow Skopeo to inherit Docker auth config ([9bb198e](https://gitlab.com/to-be-continuous/docker/commit/9bb198ef051fc53decf5e7a79970c6a1f5204567))

# [5.13.0](https://gitlab.com/to-be-continuous/docker/compare/5.12.1...5.13.0) (2024-09-15)


### Features

* **trivy:** enable comprehensive priority ([d26a665](https://gitlab.com/to-be-continuous/docker/commit/d26a6659d9c184455f4c6473154621a50af4b6e9))

## [5.12.1](https://gitlab.com/to-be-continuous/docker/compare/5.12.0...5.12.1) (2024-09-12)


### Bug Fixes

* wrong default value for DOCKER_TRIVY_ARGS ([1b84a79](https://gitlab.com/to-be-continuous/docker/commit/1b84a792b86bcae5dec8e9a4c4d9f35deb55ee98))

# [5.12.0](https://gitlab.com/to-be-continuous/docker/compare/5.11.1...5.12.0) (2024-08-26)


### Features

* introduce variable for additional docker/buildah push arguments ([9de48b2](https://gitlab.com/to-be-continuous/docker/commit/9de48b24c1512cb9f5c8b7e26d33cea1bc5504e2))

## [5.11.1](https://gitlab.com/to-be-continuous/docker/compare/5.11.0...5.11.1) (2024-08-13)


### Bug Fixes

* **build:** support metadata labels and build command arguments containing spaces ([ac391c3](https://gitlab.com/to-be-continuous/docker/commit/ac391c3aa1b7811abddeef0e68a2916669687036))

# [5.11.0](https://gitlab.com/to-be-continuous/docker/compare/5.10.3...5.11.0) (2024-07-05)


### Features

* display tools' version ([9fa5118](https://gitlab.com/to-be-continuous/docker/commit/9fa51183755b94e02af9a3151eccc5ba9be75b15))

## [5.10.3](https://gitlab.com/to-be-continuous/docker/compare/5.10.2...5.10.3) (2024-07-01)


### Bug Fixes

* **Trivy:** Trivy 0.53.0 added the clean subcommand for semantic cache management ([e3a9540](https://gitlab.com/to-be-continuous/docker/commit/e3a954080b1150ae35c403cffdb71ae750c9a741))

## [5.10.2](https://gitlab.com/to-be-continuous/docker/compare/5.10.1...5.10.2) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([6460d7b](https://gitlab.com/to-be-continuous/docker/commit/6460d7bba7a231ff68b163c861a4b40f37ee08bb))

## [5.10.1](https://gitlab.com/to-be-continuous/docker/compare/5.10.0...5.10.1) (2024-04-03)


### Bug Fixes

* **variants:** use service containers "latest" image tag (vault, GCP & AWS) ([0f57cdd](https://gitlab.com/to-be-continuous/docker/commit/0f57cdd75c71dc2d3e76d9676ce13cb11ea53362))

# [5.10.0](https://gitlab.com/to-be-continuous/docker/compare/5.9.1...5.10.0) (2024-04-02)


### Features

* **trivy:** enable custom Trivy Java DB repository ([059fda8](https://gitlab.com/to-be-continuous/docker/commit/059fda870eb99b18df835404971e25063c81c7f5))

## [5.9.1](https://gitlab.com/to-be-continuous/docker/compare/5.9.0...5.9.1) (2024-03-29)


### Bug Fixes

* kaniko-snapshot-image-cache input no longer dependent on another input ([9c51d09](https://gitlab.com/to-be-continuous/docker/commit/9c51d0945a0334146530499299ced9828dbeaae5))

# [5.9.0](https://gitlab.com/to-be-continuous/docker/compare/5.8.2...5.9.0) (2024-03-28)


### Features

* **trivy:** add variable for setting trivy db repository path ([9b2bd78](https://gitlab.com/to-be-continuous/docker/commit/9b2bd783a646c559b4097fc1e97ad73386359de4))

## [5.8.2](https://gitlab.com/to-be-continuous/docker/compare/5.8.1...5.8.2) (2024-03-21)


### Bug Fixes

* keep default dependencies on jobs using DOCKER_SNAPSHOT_IMAGE or DOCKER_RELEASE_IMAGE ([bb974c2](https://gitlab.com/to-be-continuous/docker/commit/bb974c288dc1a45dd89782f5de4ef7a928a3ffd5))

## [5.8.1](https://gitlab.com/to-be-continuous/docker/compare/5.8.0...5.8.1) (2024-1-28)


### Bug Fixes

* **sbom:** fix default Syft options ([4b03224](https://gitlab.com/to-be-continuous/docker/commit/4b0322478dfd21c24881a6832de89c5efaf50b50))

# [5.8.0](https://gitlab.com/to-be-continuous/docker/compare/5.7.1...5.8.0) (2024-1-27)


### Features

* migrate to GitLab CI/CD component ([60f2c3f](https://gitlab.com/to-be-continuous/docker/commit/60f2c3f60178a74660a4b057a054b3914cf56922))

## [5.7.1](https://gitlab.com/to-be-continuous/docker/compare/5.7.0...5.7.1) (2024-1-18)


### Bug Fixes

* Resolve "Syft packages is now deprecated" ([63a9850](https://gitlab.com/to-be-continuous/docker/commit/63a98503e02103672991de314b30ee987a81b729))

# [5.7.0](https://gitlab.com/to-be-continuous/docker/compare/5.6.1...5.7.0) (2023-12-8)


### Features

* use centralized service images (gitlab.com) ([d218fff](https://gitlab.com/to-be-continuous/docker/commit/d218fff9b6f0f2d590a4e6f738a8c01631feb56b))

## [5.6.1](https://gitlab.com/to-be-continuous/docker/compare/5.6.0...5.6.1) (2023-12-7)


### Bug Fixes

* **sbom:** syft catalogers renamed ([c3555e3](https://gitlab.com/to-be-continuous/docker/commit/c3555e325794dcd5b58f3699980cde6da7df5e6d))

# [5.6.0](https://gitlab.com/to-be-continuous/docker/compare/5.5.6...5.6.0) (2023-12-4)


### Features

* build cache can be disabled (configurable; non-default) ([fe52e0e](https://gitlab.com/to-be-continuous/docker/commit/fe52e0ebd9f851c7976f240942e9917d396bc813))

## [5.5.6](https://gitlab.com/to-be-continuous/docker/compare/5.5.5...5.5.6) (2023-12-2)


### Bug Fixes

* envsubst when variable contains a '&' ([f5781c8](https://gitlab.com/to-be-continuous/docker/commit/f5781c83e2234aba550f8cffafd808602940ff1d))

## [5.5.5](https://gitlab.com/to-be-continuous/docker/compare/5.5.4...5.5.5) (2023-11-12)


### Bug Fixes

* ECR and GCP provider image variables ([49cc61c](https://gitlab.com/to-be-continuous/docker/commit/49cc61c94a3ef70ea7ca581d2af87a2d49632148))

## [5.5.4](https://gitlab.com/to-be-continuous/docker/compare/5.5.3...5.5.4) (2023-11-03)


### Bug Fixes

* handle port number in docker release image uri ([35e1b52](https://gitlab.com/to-be-continuous/docker/commit/35e1b522d3b154da3560a73e47e350748705d42b))

## [5.5.3](https://gitlab.com/to-be-continuous/docker/compare/5.5.2...5.5.3) (2023-10-18)


### Bug Fixes

* **hadolint:** Hadolint report file name derives from Dockerfile's path (MD5) instead of content ([e7bbdcc](https://gitlab.com/to-be-continuous/docker/commit/e7bbdcc91f6fc1383d1b3e6fcdf536e2a6edb536))
* **sbom:** sbom report's name derives from snapshot image (same as Trivy) ([62341f4](https://gitlab.com/to-be-continuous/docker/commit/62341f44b76e7ac933dd518c880479c5ff3300cf))

## [5.5.2](https://gitlab.com/to-be-continuous/docker/compare/5.5.1...5.5.2) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([cce769d](https://gitlab.com/to-be-continuous/docker/commit/cce769dbe6050c84ecea1a79eac446b4766ad997))

## [5.5.1](https://gitlab.com/to-be-continuous/docker/compare/5.5.0...5.5.1) (2023-10-06)


### Bug Fixes

* **trivy:** fail when scanning an image that has reached EOL ([b89f06e](https://gitlab.com/to-be-continuous/docker/commit/b89f06e5c5786461ce225e13d07b33b0d3f1cd4d))

# [5.5.0](https://gitlab.com/to-be-continuous/docker/compare/5.4.1...5.5.0) (2023-09-30)


### Features

* **ecr:** add Amazon ECR variant ([7b0b1d9](https://gitlab.com/to-be-continuous/docker/commit/7b0b1d966c4d0c88152d0bda40715a605f712b2b))

## [5.4.1](https://gitlab.com/to-be-continuous/docker/compare/5.4.0...5.4.1) (2023-09-27)


### Bug Fixes

* **cache-repo:** switch back to kaniko default behavior ([6e15c06](https://gitlab.com/to-be-continuous/docker/commit/6e15c0621030778b03b7ddf4cfa9635f672f970d))

# [5.4.0](https://gitlab.com/to-be-continuous/docker/compare/5.3.1...5.4.0) (2023-09-22)


### Features

* **publish:** support extra tags ([b78c4e6](https://gitlab.com/to-be-continuous/docker/commit/b78c4e6a0ed7b84c8d4d568e3bca788c4695a33b))

## [5.3.1](https://gitlab.com/to-be-continuous/docker/compare/5.3.0...5.3.1) (2023-09-19)


### Bug Fixes

* **dind:** wait for Docker daemon to be ready with a timeout (1 min) ([ea965f6](https://gitlab.com/to-be-continuous/docker/commit/ea965f6e179cade7768415432593fa8a24c68162))

# [5.3.0](https://gitlab.com/to-be-continuous/docker/compare/5.2.2...5.3.0) (2023-08-28)


### Features

* **oidc:** OIDC authentication support now requires explicit configuration(see doc) ([521f918](https://gitlab.com/to-be-continuous/docker/commit/521f918b9f8fab2d23a021211bbdbfacff152c08))

## [5.2.2](https://gitlab.com/to-be-continuous/docker/compare/5.2.1...5.2.2) (2023-07-25)


### Bug Fixes

* **kaniko:** Allow repositories with port numbers to be used for caching ([d17e215](https://gitlab.com/to-be-continuous/docker/commit/d17e215c424fae9b9befc23922f93e23ef273191))

## [5.2.1](https://gitlab.com/to-be-continuous/docker/compare/5.2.0...5.2.1) (2023-07-12)


### Bug Fixes

* **doc:** update typo on documentation [skip ci] ([95245ba](https://gitlab.com/to-be-continuous/docker/commit/95245ba4982663513ac4114878ed8bad0bc07a24))
* **kaniko:** force '--cache-repo' option (strip tag) ([7d4a194](https://gitlab.com/to-be-continuous/docker/commit/7d4a19461953ff81edae0252701abcbbfbc4affe))

# [5.2.0](https://gitlab.com/to-be-continuous/docker/compare/5.1.0...5.2.0) (2023-06-16)


### Features

* **gcp:** add OIDC authentication support for GCP Artifact Registry ([ecac4a6](https://gitlab.com/to-be-continuous/docker/commit/ecac4a692487170adeeccbfdc5d7a97e195c98f2))

# [5.1.0](https://gitlab.com/to-be-continuous/docker/compare/5.0.3...5.1.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([43ab7e7](https://gitlab.com/to-be-continuous/docker/commit/43ab7e77f94fbd36cf0e0845c5d6efa78c89621b))

## [5.0.3](https://gitlab.com/to-be-continuous/docker/compare/5.0.2...5.0.3) (2023-05-17)


### Bug Fixes

* derive buildah cache from snapshot image ([63016e6](https://gitlab.com/to-be-continuous/docker/commit/63016e61044bf96ba5f1ff239b5e4db6ac2e5b92))

## [5.0.2](https://gitlab.com/to-be-continuous/docker/compare/5.0.1...5.0.2) (2023-05-16)


### Bug Fixes

* **kaniko:** fix $HOME variable ([e213a9e](https://gitlab.com/to-be-continuous/docker/commit/e213a9e24c6712b04af75421ff03e0cf4a52dd34))

## [5.0.1](https://gitlab.com/to-be-continuous/docker/compare/5.0.0...5.0.1) (2023-05-15)


### Bug Fixes

* use $HOME for skopeo credentials for rootless use ([e8b89fd](https://gitlab.com/to-be-continuous/docker/commit/e8b89fdeaada7353998d3b92668873af9cb46b87))

# [5.0.0](https://gitlab.com/to-be-continuous/docker/compare/4.0.0...5.0.0) (2023-05-11)


### Features

* add buildah support for building images ([f8de563](https://gitlab.com/to-be-continuous/docker/commit/f8de56361f8659d8d42144b0b4f26f3bcdf769d0))


### BREAKING CHANGES

* $DOCKER_DIND_BUILD no longer supported (replaced by $`DOCKER_BUILD_TOOL`)

# [4.0.0](https://gitlab.com/to-be-continuous/docker/compare/3.5.3...4.0.0) (2023-04-05)


### Features

* **publish:** redesign publish on prod strategy ([524ccc1](https://gitlab.com/to-be-continuous/docker/commit/524ccc10686991d63aa6f23b40a9b77f68da4cb9))


### BREAKING CHANGES

* **publish:** $PUBLISH_ON_PROD no longer supported (replaced by $DOCKER_PROD_PUBLISH_STRATEGY - see doc)

## [3.5.3](https://gitlab.com/to-be-continuous/docker/compare/3.5.2...3.5.3) (2023-03-28)


### Bug Fixes

* **sbom:** add CycloneDX report ([76c6727](https://gitlab.com/to-be-continuous/docker/commit/76c6727052690354c03604193a6f1ff53bc34e10))

## [3.5.2](https://gitlab.com/to-be-continuous/docker/compare/3.5.1...3.5.2) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([b45e6a2](https://gitlab.com/to-be-continuous/docker/commit/b45e6a2b9cd760a7552fee3c2646a6db91871744))

## [3.5.1](https://gitlab.com/to-be-continuous/docker/compare/3.5.0...3.5.1) (2023-01-04)


### Bug Fixes

* wrong $docker_image_digest if GitLab registry host has a port ([c121756](https://gitlab.com/to-be-continuous/docker/commit/c121756cbf6e24d9890d11720c0a8b43a5fbb902))

# [3.5.0](https://gitlab.com/to-be-continuous/docker/compare/3.4.0...3.5.0) (2022-12-13)


### Features

* **vault:** configurable Vault Secrets Provider images ([96e2c5d](https://gitlab.com/to-be-continuous/docker/commit/96e2c5df04f633b86ac9463782c37a7dd4e3a549))

# [3.4.0](https://gitlab.com/to-be-continuous/docker/compare/3.3.0...3.4.0) (2022-12-12)


### Features

* semantic release integration ([fabf9b9](https://gitlab.com/to-be-continuous/docker/commit/fabf9b9b3a9c93d08e89b940f22566e7670ee8be))

# [3.3.0](https://gitlab.com/to-be-continuous/docker/compare/3.2.2...3.3.0) (2022-11-29)


### Features

* add a job generating software bill of materials ([ea8ca4e](https://gitlab.com/to-be-continuous/docker/commit/ea8ca4e7c5904e34dda2ee6fb6b33788f4b4544a))

## [3.2.2](https://gitlab.com/to-be-continuous/docker/compare/3.2.1...3.2.2) (2022-10-06)


### Bug Fixes

* Error when generating Trivy report ([2b5ec6e](https://gitlab.com/to-be-continuous/docker/commit/2b5ec6e5fe570b14d0747eb6b65f0f086f1ca75f))

## [3.2.1](https://gitlab.com/to-be-continuous/docker/compare/3.2.0...3.2.1) (2022-10-04)


### Bug Fixes

* **hadolint:** fix shell syntax error ([88de431](https://gitlab.com/to-be-continuous/docker/commit/88de43147892124c85ef6f62a381c8ead7e2929b))

# [3.2.0](https://gitlab.com/to-be-continuous/docker/compare/3.1.1...3.2.0) (2022-10-04)


### Features

* normalize reports ([e8d505f](https://gitlab.com/to-be-continuous/docker/commit/e8d505fea056148389744831a09e9fca978f8f38))

## [3.1.1](https://gitlab.com/to-be-continuous/docker/compare/3.1.0...3.1.1) (2022-09-28)


### Bug Fixes

* support DOCKER_XX_IMAGE with registry port ([54e9e05](https://gitlab.com/to-be-continuous/docker/commit/54e9e05f810b8abb81440cba7dd8ebd7e89644a9))

# [3.1.0](https://gitlab.com/to-be-continuous/docker/compare/3.0.0...3.1.0) (2022-09-20)


### Features

* add custom DOCKER_CONFIG_FILE var ([3201240](https://gitlab.com/to-be-continuous/docker/commit/3201240e9437d7602779fb33252e8dce7e028257))

# [3.0.0](https://gitlab.com/to-be-continuous/docker/compare/2.7.1...3.0.0) (2022-08-05)


### Bug Fixes

* trigger new major release ([580d3e3](https://gitlab.com/to-be-continuous/docker/commit/580d3e3ef93ab570f30b7c90216301fefbdbf84e))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

## [2.7.1](https://gitlab.com/to-be-continuous/docker/compare/2.7.0...2.7.1) (2022-06-20)


### Bug Fixes

* **skopeo:** authenticate with skopeo inspect ([53cf10d](https://gitlab.com/to-be-continuous/docker/commit/53cf10d42aa268650b0b61705f8b241eb3e7d2b4))
* **Trivy:** prefix Trivy report name ([4cec06b](https://gitlab.com/to-be-continuous/docker/commit/4cec06bb5731ced82e3a9fdecfdd82eb54378822))

# [2.7.0](https://gitlab.com/to-be-continuous/docker/compare/2.6.0...2.7.0) (2022-05-20)


### Features

* **Trivy:** allow Trivy to be run in standalone mode ([88348c8](https://gitlab.com/to-be-continuous/docker/commit/88348c8bab10eaf7b2e732ee6c018f50f587af9e))

# [2.6.0](https://gitlab.com/to-be-continuous/docker/compare/2.5.0...2.6.0) (2022-05-19)


### Features

* Make the --vuln-type Trivy argument configurable ([15457c6](https://gitlab.com/to-be-continuous/docker/commit/15457c6e8574b2f4ab4c22163f11118382fa27a2))

# [2.5.0](https://gitlab.com/to-be-continuous/docker/compare/2.4.0...2.5.0) (2022-05-01)


### Features

* configurable tracking image ([b91e936](https://gitlab.com/to-be-continuous/docker/commit/b91e936e028c8002edd8f79beb20d7451c87ead4))

# [2.4.0](https://gitlab.com/to-be-continuous/docker/compare/2.3.3...2.4.0) (2022-04-27)


### Features

* add image digest support ([57998b2](https://gitlab.com/to-be-continuous/docker/commit/57998b26c37086faee0b5524d31917f4f4a3ce53))

## [2.3.3](https://gitlab.com/to-be-continuous/docker/compare/2.3.2...2.3.3) (2022-04-12)


### Bug Fixes

* **Trivy:** restore default Git strategy to allow .trivyignore ([b2fc514](https://gitlab.com/to-be-continuous/docker/commit/b2fc51491d875d65a4998dfa47f124ba11a50615))

## [2.3.2](https://gitlab.com/to-be-continuous/docker/compare/2.3.1...2.3.2) (2022-02-24)


### Bug Fixes

* **vault:** revert Vault JWT authentication not working ([b7ac60a](https://gitlab.com/to-be-continuous/docker/commit/b7ac60a595f6a27b3fc0d24c10465ac923b196d0))

## [2.3.1](https://gitlab.com/to-be-continuous/docker/compare/2.3.0...2.3.1) (2022-02-23)


### Bug Fixes

* **vault:** Vault JWT authentication not working ([4949a87](https://gitlab.com/to-be-continuous/docker/commit/4949a874fb96dedceb0fb16dcd78693d6351dec0))

# [2.3.0](https://gitlab.com/to-be-continuous/docker/compare/2.2.0...2.3.0) (2022-01-10)


### Features

* Vault variant + non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([f1bbac3](https://gitlab.com/to-be-continuous/docker/commit/f1bbac38b305f8bdda946d8372338c5293da7f59))

# [2.2.0](https://gitlab.com/to-be-continuous/docker/compare/2.1.2...2.2.0) (2021-11-24)


### Features

* add configurable metadata variable with OCI recommended labels ([d3630f9](https://gitlab.com/to-be-continuous/docker/commit/d3630f9356ba3c9934f972ca728f562e3d015019))

## [2.1.2](https://gitlab.com/to-be-continuous/docker/compare/2.1.1...2.1.2) (2021-10-19)


### Bug Fixes

* **trivy:** ignore unfixed security issues by default ([f9a1602](https://gitlab.com/to-be-continuous/docker/commit/f9a160201fe11a1ac7d125b5fa9aa181c4599fd5))

## [2.1.1](https://gitlab.com/to-be-continuous/docker/compare/2.1.0...2.1.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([5596daf](https://gitlab.com/to-be-continuous/docker/commit/5596daf34cc9c64d73405ef490fe75c7ae50a177))

## [2.0.0](https://gitlab.com/to-be-continuous/docker/compare/1.2.3...2.0.0) (2021-09-02)

### Features

* Change boolean variable behaviour ([f74d9ef](https://gitlab.com/to-be-continuous/docker/commit/f74d9ef2de5b4dc204b519e14c47862ea2b73b33))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.3](https://gitlab.com/to-be-continuous/docker/compare/1.2.2...1.2.3) (2021-07-21)

### Bug Fixes

* variable name ([1aed76f](https://gitlab.com/to-be-continuous/docker/commit/1aed76f287ff27e35233941e35cbac1e8ea9d2ff))

## [1.2.2](https://gitlab.com/to-be-continuous/docker/compare/1.2.1...1.2.2) (2021-07-05)

### Bug Fixes

* update skopeo credentials ([559961f](https://gitlab.com/to-be-continuous/docker/commit/559961f9f3c30e241a049ee4ea94e9050be49592))

## [1.2.1](https://gitlab.com/to-be-continuous/docker/compare/1.2.0...1.2.1) (2021-06-25)

### Bug Fixes

* permission on reports directory ([2d2f360](https://gitlab.com/to-be-continuous/docker/commit/2d2f360420b13bbdb71c11910ac8214b74a15901))

## [1.2.0](https://gitlab.com/to-be-continuous/docker/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([405fdcc](https://gitlab.com/to-be-continuous/docker/commit/405fdcc65ef1e95cb5997c610266c1422a06802e))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/docker/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([fc9a1ad](https://gitlab.com/Orange-OpenSource/tbc/docker/commit/fc9a1adc498209ea9fa7c6eb64831f6e1abe857f))

## 1.0.0 (2021-05-06)

### Features

* initial release ([0b00fba](https://gitlab.com/Orange-OpenSource/tbc/docker/commit/0b00fba9a515d4ea10c35cf38abc01c217f0979a))
